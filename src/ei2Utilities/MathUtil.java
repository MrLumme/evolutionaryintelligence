package ei2Utilities;

public class MathUtil {

	public static float map(float value, float minIn, float maxIn, float minOut, float maxOut) {
		return (value - minIn) * (maxOut - minOut) / (maxIn - minIn) + minOut;
	}

	public static int map(int value, int minIn, int maxIn, int minOut, int maxOut) {
		return (value - minIn) * (maxOut - minOut) / (maxIn - minIn) + minOut;
	}

	public static long map(long value, long minIn, long maxIn, long minOut, long maxOut) {
		return (value - minIn) * (maxOut - minOut) / (maxIn - minIn) + minOut;
	}

}
