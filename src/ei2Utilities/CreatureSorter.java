package ei2Utilities;

import java.util.ArrayList;

import ei2Core.Creature;
import ei2Types.CreatureSort;

public class CreatureSorter {

	// ----
	// SORT
	// ----

	public static Creature[] sort(Creature[] c, CreatureSort sortFor, boolean sortHighest) {
		Creature[] set = mergeAndSort(c, sortFor);

		if (sortHighest) {
			Creature[] temp = new Creature[set.length];
			for (int i = 0; i < set.length; i++) {
				temp[i] = set[set.length - 1 - i];
			}
			set = temp;
		}

		return set;
	}

	public static ArrayList<Creature> sort(ArrayList<Creature> c, CreatureSort sortFor, boolean sortHighest) {
		ArrayList<Creature> listOut = new ArrayList<Creature>();
		Creature[] arrayIn = new Creature[c.size()];
		Creature[] arrayOut = new Creature[c.size()];

		for (int i = 0; i < c.size(); i++) {
			arrayIn[i] = c.get(i);
		}

		arrayOut = sort(arrayIn, sortFor, sortHighest);

		for (int i = 0; i < c.size(); i++) {
			listOut.add(arrayOut[i]);
			// System.out.println(i + ": " + arrayOut[i].getName());
		}

		return listOut;
	}

	// --------------
	// MERGE AND SORT
	// --------------

	private static Creature[] mergeAndSort(Creature[] c, CreatureSort sortFor) {
		Creature[] l, h;
		Creature[] f = new Creature[c.length];

		if (c.length > 1) {
			l = mergeAndSort(splitLow(c), sortFor);
			h = mergeAndSort(splitHigh(c), sortFor);

			int lp = 0, hp = 0;
			int fp = 0;

			while (fp < f.length) {
				// Pour in low
				if (lp < l.length && hp >= h.length) {
					f[fp] = l[lp];
					lp++;
				}
				// Pour in high
				else if (hp < h.length && lp >= l.length) {
					f[fp] = h[hp];
					hp++;
				}
				// Check which is smaller
				else {
					if (isLess(l[lp], h[hp], sortFor)) {
						f[fp] = l[lp];
						lp++;
					} else {
						f[fp] = h[hp];
						hp++;
					}
				}
				fp++;
			}
		} else {
			f = c;
		}
		return f;
	}

	// -------
	// IS LESS
	// -------

	private static boolean isLess(Creature c1, Creature c2, CreatureSort sortFor) {
		boolean o = false;

		switch (sortFor) {
		case AGE:
			if (c1.getAge() < c2.getAge()) {
				o = true;
			}
			break;
		case FITNESS:
			if (c1.getFitness() < c2.getFitness()) {
				o = true;
			}
			break;
		case ID:
			if (c1.getId() < c2.getId()) {
				o = true;
			}
			break;
		case NAME:
			if (c1.getName().compareTo(c2.getName()) < 0) {
				o = true;
			}
			break;
		}

		return o;
	}

	// ---------
	// SPLIT LOW
	// ---------

	private static Creature[] splitLow(Creature[] c) {
		Creature[] t = new Creature[(int) Math.floor(c.length / 2.0)];

		for (int i = 0; i < t.length; i++) {
			t[i] = c[i];
		}

		return t;
	}

	// ----------
	// SPLIT HIGH
	// ----------

	private static Creature[] splitHigh(Creature[] c) {
		Creature[] t = new Creature[(int) Math.ceil(c.length / 2.0)];

		for (int i = 0; i < t.length; i++) {
			t[i] = c[i + (c.length - t.length)];
		}

		return t;
	}
}
