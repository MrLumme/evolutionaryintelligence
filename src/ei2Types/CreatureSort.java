package ei2Types;

public enum CreatureSort {

	// ----------
	// SORT TYPES
	// ----------

	ID, NAME, AGE, FITNESS;

	// --------------
	// SORT FUNCTIONS
	// --------------

	public static CreatureSort getNext(CreatureSort c) {
		CreatureSort o = null;

		switch (c) {
		case ID:
			o = NAME;
			break;
		case NAME:
			o = AGE;
			break;
		case AGE:
			o = FITNESS;
			break;
		case FITNESS:
			o = ID;
			break;
		}

		return o;
	}
}
