package ei2Types;

import ei2Core.Main;
import processing.core.PApplet;

public final class ColorTypes {

	private final static PApplet p = Main.instance;

	// -----------
	// COLOR TYPES
	// -----------

	public static final int WHITE = p.color(255);
	public static final int L_GREY = p.color(191);
	public static final int GREY = p.color(127);
	public static final int D_GREY = p.color(63);
	public static final int BLACK = p.color(0);

	public static final int BLUE = p.color(0, 0, 255);
	public static final int GREEN = p.color(0, 255, 0);
	public static final int RED = p.color(255, 0, 0);

	public static final int MAGENTA = p.color(255, 0, 255);
	public static final int CYAN = p.color(0, 255, 255);
	public static final int YELLOW = p.color(255, 255, 0);

	public static final int TURQUOISE = p.color(160, 255, 0);
	public static final int PURPLE = p.color(160, 0, 255);
	public static final int ORANGE = p.color(255, 160, 0);

	public static final int LIME = p.color(160, 255, 0);
	public static final int NAVY = p.color(0, 160, 255);
	public static final int PINK = p.color(255, 0, 160);

}
