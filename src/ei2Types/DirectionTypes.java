package ei2Types;

public final class DirectionTypes {

	// ---------------
	// DIRECTION TYPES
	// ---------------

	public static final byte RIGHT_DIR = 0;
	public static final byte DOWN_DIR = 1;
	public static final byte LEFT_DIR = 2;
	public static final byte UP_DIR = 3;

	// --------------
	// TYPE CONSTANTS
	// --------------

	public static final byte DIR_AMOUNT = 4;
	public static final byte[][] DIR_PRESET = { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 } };
	public static final String[] DIR_NAME = { "right", "down", "left", "up" };

	// --------------
	// TYPE FUNCTIONS
	// --------------

	public static byte getRandomDir() {
		return (byte) Math.floor(Math.random() * DIR_AMOUNT);
	}
}