package ei2Types;

public enum CreatureSetting {

	// -----------------
	// CREATURE SETTINGS
	// -----------------

	NONE, REPRODUCTION_CHANCE, MUTATION_CHANCE, CHILD_MUTATIONS, SPAWN_MUTATIONS, CREATURE_COUNT;

	// -----------------
	// SETTING FUNCTIONS
	// -----------------

	public static CreatureSetting getNext(CreatureSetting c) {
		CreatureSetting s = NONE;

		switch (c) {
		case NONE:
			s = REPRODUCTION_CHANCE;
			break;
		case REPRODUCTION_CHANCE:
			s = MUTATION_CHANCE;
			break;
		case MUTATION_CHANCE:
			s = CHILD_MUTATIONS;
			break;
		case CHILD_MUTATIONS:
			s = SPAWN_MUTATIONS;
			break;
		case SPAWN_MUTATIONS:
			s = CREATURE_COUNT;
			break;
		case CREATURE_COUNT:
			s = NONE;
			break;
		}

		return s;
	}

}
