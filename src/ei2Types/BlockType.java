package ei2Types;

public enum BlockType {

	// -----------
	// BLOCK TYPES
	// -----------

	EMPTY, WALL, MUD;

	// ---------------
	// BLOCK CONSTANTS
	// ---------------

	public static final byte BLOCK_AMOUNT = 3;
	public static final String[] BLOCK_NAME = { "Empty", "Wall", "Mud" };

	// --------------
	// TYPE FUNCTIONS
	// --------------

	public static BlockType getRandomType() {
		BlockType a;

		byte select = (byte) Math.floor(Math.random() * BLOCK_AMOUNT);
		switch (select) {
		case 1:
			a = WALL;
			break;
		case 2:
			a = MUD;
			break;
		default:
			a = EMPTY;
			break;
		}

		return a;
	}

	public static String getTypeName(BlockType a) {
		String out = "";

		switch (a) {
		case WALL:
			out = BLOCK_NAME[1];
			break;
		case MUD:
			out = BLOCK_NAME[2];
			break;
		default:
			out = BLOCK_NAME[0];
			break;
		}

		return out;
	}

	public static BlockType getNext(BlockType b) {
		BlockType t = EMPTY;

		switch (b) {
		case EMPTY:
			t = WALL;
			break;
		case WALL:
			t = MUD;
			break;
		case MUD:
			t = EMPTY;
			break;
		}

		return t;
	}
}
