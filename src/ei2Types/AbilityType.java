package ei2Types;

public enum AbilityType {

	// -------------
	// ABILITY TYPES
	// -------------

	UNRESPONSIVE, WALK, EAT, ATTACK, BUILD;

	// --------------
	// TYPE CONSTANTS
	// --------------

	private static final byte ABILITY_AMOUNT = 5;
	private static final String ABILITY_NAME[] = { "Unrespon", "Walk", "Eat", "Attack", "Build" };

	// --------------
	// TYPE FUNCTIONS
	// --------------

	public static AbilityType getRandomType() {
		AbilityType a;

		byte select = (byte) Math.floor(Math.random() * ABILITY_AMOUNT);
		switch (select) {
		case 1:
			a = WALK;
			break;
		case 2:
			a = EAT;
			break;
		case 3:
			a = ATTACK;
			break;
		case 4:
			a = BUILD;
			break;
		default:
			a = UNRESPONSIVE;
			break;
		}

		return a;
	}

	public static String getTypeName(AbilityType a) {
		String out = "";

		switch (a) {
		case WALK:
			out = ABILITY_NAME[1];
			break;
		case EAT:
			out = ABILITY_NAME[2];
			break;
		case ATTACK:
			out = ABILITY_NAME[3];
			break;
		case BUILD:
			out = ABILITY_NAME[4];
			break;
		default:
			out = ABILITY_NAME[0];
			break;
		}

		return out;
	}
}
