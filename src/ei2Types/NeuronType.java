package ei2Types;

public enum NeuronType {

	// ------------
	// NEURON TYPES
	// ------------

	UNRESPONSIVE, SEE_WALL, SEE_CREATURE, SEE_PLANT, SEE_FOOD, TASTE, THINK;

	// --------------
	// TYPE CONSTANTS
	// --------------

	private static final byte NEURON_AMOUNT = 7;
	private static final String NEURON_NAME[] = { "Unrespon", "See wall", "See creature", "See plant", "See food",
			"Taste", "Think" };

	// --------------
	// TYPE FUNCTIONS
	// --------------

	public static NeuronType getRandomType() {
		NeuronType n;

		byte select = (byte) Math.floor(Math.random() * NEURON_AMOUNT);
		switch (select) {
		case 1:
			n = SEE_WALL;
			break;
		case 2:
			n = SEE_CREATURE;
			break;
		case 3:
			n = SEE_PLANT;
			break;
		case 4:
			n = SEE_FOOD;
			break;
		case 5:
			n = TASTE;
			break;
		case 6:
			n = THINK;
			break;
		default:
			n = UNRESPONSIVE;
			break;
		}

		return n;
	}

	public static String getTypeName(NeuronType a) {
		String out = "";

		switch (a) {
		case SEE_WALL:
			out = NEURON_NAME[1];
			break;
		case SEE_CREATURE:
			out = NEURON_NAME[2];
			break;
		case SEE_PLANT:
			out = NEURON_NAME[3];
			break;
		case SEE_FOOD:
			out = NEURON_NAME[4];
			break;
		case TASTE:
			out = NEURON_NAME[5];
			break;
		case THINK:
			out = NEURON_NAME[6];
			break;
		default:
			out = NEURON_NAME[0];
			break;
		}

		return out;
	}
}
