package ei2Types;

public enum ProgramState {

	// --------------
	// PROGRAM STATES
	// --------------

	MAIN_MENU, MAP, SIMULATION, GENERATION, SETUP;
}
