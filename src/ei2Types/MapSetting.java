package ei2Types;

public enum MapSetting {

	// -----------
	// MAP SETTING
	// -----------

	NONE, WALL_DENSITY, MUD_DENSITY, PLANT_DENSITY;

	// -----------------
	// SETTING FUNCTIONS
	// -----------------

	public static MapSetting getNext(MapSetting mapSetting) {
		MapSetting s = NONE;

		switch (mapSetting) {
		case NONE:
			s = WALL_DENSITY;
			break;
		case WALL_DENSITY:
			s = MUD_DENSITY;
			break;
		case MUD_DENSITY:
			s = PLANT_DENSITY;
			break;
		case PLANT_DENSITY:
			s = NONE;
			break;
		}

		return s;
	}

}
