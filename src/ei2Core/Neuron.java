package ei2Core;

import ei2Types.BlockType;
import ei2Types.DirectionTypes;
import ei2Types.NeuronType;

public class Neuron {

	private NeuronType type;

	private byte direction;
	private byte consumption;
	private boolean negate;

	private Ability reaction;
	private Creature vessel;

	// -----------
	// CONSTRUCTOR
	// -----------

	public Neuron(Creature vessel) {
		this.type = NeuronType.SEE_WALL; // NeuronType.getRandomType();
		this.direction = (byte) Math.floor(Math.random() * (DirectionTypes.DIR_AMOUNT));
		this.negate = (Math.random() > 0.50) ? true : false;

		this.reaction = new Ability(vessel);
		this.vessel = vessel;
	}

	public Neuron(NeuronType type, byte direction, boolean negate, Ability reaction, Creature vessel) {
		this.type = type;
		this.direction = direction;
		this.negate = negate;

		this.reaction = reaction.copy(vessel);
		this.vessel = vessel;
	}

	// ----
	// STEP
	// ----

	public boolean step() {
		boolean hasActed = false;

		switch (type) {
		case SEE_WALL:
			int x = vessel.getX() + DirectionTypes.DIR_PRESET[direction][0];
			int y = vessel.getY() + DirectionTypes.DIR_PRESET[direction][1];

			if (negate) {
				if (!Main.world.isBlockAt(x, y, BlockType.WALL)) {
					hasActed = reaction.step();
				}
			} else {
				if (Main.world.isBlockAt(x, y, BlockType.WALL)) {
					hasActed = reaction.step();
				}
			}
			break;
		case SEE_CREATURE:
			break;
		case SEE_PLANT:
			break;
		case SEE_FOOD:
			break;
		case TASTE:
			break;
		case THINK:
			break;
		default:
			break;
		}

		return hasActed;
	}

	// ----
	// COPY
	// ----

	public Neuron copy(Creature vessel) {
		return new Neuron(type, direction, negate, reaction, vessel);
	}

	// -----------
	// GET AND SET
	// -----------

	public NeuronType getType() {
		return type;
	}

	public void setType(NeuronType type) {
		this.type = type;
	}

	public byte getDirection() {
		return direction;
	}

	public void setDirection(byte direction) {
		this.direction = direction;
	}

	public boolean isNegate() {
		return negate;
	}

	public void setNegate(boolean negate) {
		this.negate = negate;
	}

	public Ability getReaction() {
		return reaction;
	}

	public void setReaction(Ability reaction) {
		this.reaction = reaction;
	}

}
