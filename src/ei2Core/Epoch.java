package ei2Core;

import java.util.ArrayList;
import java.util.Iterator;

import ei2Types.CreatureSort;
import ei2Utilities.CreatureSorter;

public class Epoch {
	private ArrayList<Creature> creatures;

	// -----------
	// CONSTRUCTOR
	// -----------

	public Epoch() {
		creatures = new ArrayList<>();
	}

	// ---
	// ADD
	// ---

	public boolean add(Creature c) {
		return creatures.add(c);
	}

	// ------
	// REMOVE
	// ------

	public boolean remove(Creature c) {
		return creatures.remove(c);
	}

	// ---
	// GET
	// ---

	public Creature get(int i) {
		return creatures.get(i);
	}

	// ----
	// SIZE
	// ----

	public int size() {
		return creatures.size();
	}

	// ----
	// SORT
	// ----

	public void sort(CreatureSort sortFor, boolean sortHighest) {
		creatures = CreatureSorter.sort(creatures, sortFor, sortHighest);
	}

	// --------
	// ITERATOR
	// --------

	public Iterator<Creature> iterator() {
		return creatures.iterator();
	}
}
