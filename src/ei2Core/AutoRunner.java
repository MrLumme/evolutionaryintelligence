package ei2Core;

import ei2Types.ProgramState;
import processing.core.PApplet;

public class AutoRunner {

	private static PApplet p = Main.instance;

	private static AutoRunner instance;

	private static int delay;
	private static long lastTimer;
	private static boolean running;
	private static byte index;

	// -----------
	// CONSTRUCTOR
	// -----------

	public AutoRunner() {
		lastTimer = p.millis();
		running = false;
		delay = 1000;
	}

	// ------------
	// GET INSTANCE
	// ------------

	public static AutoRunner getInstance() {
		if (instance == null) {
			instance = new AutoRunner();
		}
		return instance;
	}

	// ----
	// STEP
	// ----

	public static void step() {
		if (running && p.millis() > lastTimer + delay) {
			stepEvent();
			lastTimer = p.millis();
		}
	}

	// ----------
	// STEP EVENT
	// ----------

	private static void stepEvent() {
		if (Main.ps == ProgramState.GENERATION) {
			if (index == 0) {
				Main.world.sortGeneration(Main.sortFor, Main.sortHighest);
			} else if (index == 1) {
				Main.world.autoSelect();
			} else if (index == 2) {
				Main.world.killSelection();
			} else if (index == 3) {
				Main.world.newGeneration();
			} else if (index == 4) {
				if (Main.world.isFinished() == false) {
					Main.ps = ProgramState.SIMULATION;
					Main.world.simulate();
				}
			}
			index = (byte) ((index + 1) % 5);
		}
	}

	// -----------
	// GET AND SET
	// -----------

	public static boolean isRunning() {
		return running;
	}

	public static void setRunning(boolean running) {
		AutoRunner.running = running;
	}
}
