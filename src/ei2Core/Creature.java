package ei2Core;

import java.util.ArrayList;

import ei2Types.AbilityType;
import ei2Types.ColorTypes;
import ei2Types.DirectionTypes;
import ei2Types.NeuronType;
import processing.core.PApplet;

public class Creature extends Position {

	private static int idCounter = 0;

	// Creature settings
	public static float REPRODUCTION_CHANCE = 0.50f;
	public static float MUTATION_CHANCE = 0.50f;
	public static int CHILD_MUTATIONS = 1;
	public static int SPAWN_MUTATIONS = 5;

	// --------
	// VARIBLES
	// --------

	private String name = "";
	private String dumbName;
	private int id;

	private ArrayList<Neuron> brain;
	private ArrayList<Ability> body;

	Nutrients need;
	Nutrients nutrients;

	private byte health, energy;
	private byte healthMax, energyMax;
	private boolean active;
	private boolean selected;

	private int fitness, age;
	private int generation;
	private Creature parent;

	// -----------
	// CONSTRUCTOR
	// -----------

	public Creature(int x, int y, int col) {
		super(x, y, col);
		parent = null;
		active = false;
		selected = false;

		id = idCounter++;

		brain = new ArrayList<>();
		body = new ArrayList<>();

		Nutrients need = new Nutrients();
		Nutrients nutrients = new Nutrients();

		healthMax = 10;
		health = healthMax;
		energyMax = 100;
		energy = energyMax;

		fitness = 0;
		age = 0;

		// Make name
		int l = (int) Math.round((Math.random() * 3) * (Math.random() * 3));
		name += (char) Math.floor((Math.random() * (90 - 65)) + 65);
		for (int i = 0; i < l; i++) {
			name += (char) Math.floor(Math.random() * (126 - 33) + 33);
		}
		dumbName = (Math.random() > 0.50 ? "Dumb as shit" : "Stupid as fuck");
	}

	// ----
	// STEP
	// ----

	public void step() {
		if (active) {
			boolean hasActed = false;

			for (Neuron neu : brain) {
				if (hasActed == false) {
					hasActed = neu.step();
				}
			}
			calculateFitness();
			age++;
		}
	}

	// ----
	// DRAW
	// ----

	public void draw(int x, int y, boolean showSelection) {
		if (showSelection && selected) {
			p.stroke(255);
			p.noFill();
			p.rect(x * Main.BLOCK_SIZE - (Main.BLOCK_SIZE / 4.0f), y * Main.BLOCK_SIZE - (Main.BLOCK_SIZE / 4.0f),
					Main.BLOCK_SIZE * 1.5f - 1, Main.BLOCK_SIZE * 1.5f - 1);
		}

		super.draw(x, y);
	}

	// ----
	// INFO
	// ----

	public void info() {

		// Tell Me What You Are

		p.fill(ColorTypes.WHITE);
		p.text("Name:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.text(name, (Main.WORLD_WIDTH + 4) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.text("Age:", (Main.WORLD_WIDTH + 9) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.text(age, (Main.WORLD_WIDTH + 12) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());

		InfoPanel.setInfoHeight(1.00f + InfoPanel.getInfoHeight());

		// Position

		p.text("Position:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.text("(" + x + ";" + y + ")", (Main.WORLD_WIDTH + 6) * Main.BLOCK_SIZE,
				Main.BLOCK_SIZE * InfoPanel.getInfoHeight());

		InfoPanel.setInfoHeight(1.00f + InfoPanel.getInfoHeight());

		// General States

		p.fill(p.lerpColor(ColorTypes.RED, ColorTypes.GREEN, ((float) health) / ((float) healthMax)));
		p.text("Health:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.text(health, (Main.WORLD_WIDTH + 5) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.fill(p.lerpColor(ColorTypes.BLUE, ColorTypes.YELLOW, ((float) energy) / ((float) energyMax)));
		p.text("Energy:", (Main.WORLD_WIDTH + 7) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.text(energy, (Main.WORLD_WIDTH + 12) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());

		InfoPanel.setInfoHeight(2.00f + InfoPanel.getInfoHeight());

		p.fill(ColorTypes.WHITE);
		p.text("Generation:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.text(generation, (Main.WORLD_WIDTH + 8) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());

		InfoPanel.setInfoHeight(1.00f + InfoPanel.getInfoHeight());

		p.text("Fitness:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.text(fitness, (Main.WORLD_WIDTH + 8) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());

		InfoPanel.setInfoHeight(2.00f + InfoPanel.getInfoHeight());

		// Nutrients

		// for (int i = 0; i < Nutrients.NUTRIENT_AMOUNT; i++) {
		// p.fill(Nutrients.NUTRIENT_COLOR[i]);
		// p.text(Nutrients.NUTRIENT_NAME[i], (Main.WORLD_WIDTH + (i * 2.5f)) *
		// Main.BLOCK_SIZE,
		// Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		// p.text("" + Math.round(nutrients.getNutrient((byte) i)),
		// (Main.WORLD_WIDTH + (i * 2.5f)) * Main.BLOCK_SIZE,
		// Main.BLOCK_SIZE * (InfoPanel.getInfoHeight() + 1.00f));
		// }
		//
		// p.fill(ColorTypes.WHITE);
		// InfoPanel.setInfoHeight(3.00f + InfoPanel.getInfoHeight());

		// Abilities

		// p.text("Abilities:", Main.WORLD_WIDTH * Main.BLOCK_SIZE,
		// Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		// InfoPanel.setInfoHeight(1.00f + InfoPanel.getInfoHeight());
		//
		// for (int i = 0; i < body.size(); i++) {
		// p.text(AbilityType.getTypeName(body.get(i).getType()) + " "
		// + DirectionTypes.DIR_NAME[body.get(i).direction].toLowerCase(),
		// (Main.WORLD_WIDTH + 1) * Main.BLOCK_SIZE, Main.BLOCK_SIZE *
		// InfoPanel.getInfoHeight());
		// InfoPanel.setInfoHeight(1.00f + InfoPanel.getInfoHeight());
		// }
		//
		// InfoPanel.setInfoHeight(1.00f + InfoPanel.getInfoHeight());

		// Neurons

		p.text("Neurons:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		InfoPanel.setInfoHeight(1.00f + InfoPanel.getInfoHeight());

		if (brain.size() == 0) {
			p.text(dumbName, (Main.WORLD_WIDTH + 1) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		} else {
			for (int i = 0; i < brain.size(); i++) {

				String negStr = "";
				if (brain.get(i).isNegate() == true) {
					negStr = "no ";
				}

				p.text("If " + negStr + NeuronType.getTypeName(brain.get(i).getType()).toLowerCase() + " "
						+ DirectionTypes.DIR_NAME[brain.get(i).getDirection()].toLowerCase() + " then ",
						(Main.WORLD_WIDTH + 1) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());

				InfoPanel.setInfoHeight(1.00f + InfoPanel.getInfoHeight());

				p.text(AbilityType.getTypeName(brain.get(i).getReaction().getType()).toLowerCase() + " "
						+ DirectionTypes.DIR_NAME[brain.get(i).getReaction().getDirection()].toLowerCase(),
						(Main.WORLD_WIDTH + 2) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());

				InfoPanel.setInfoHeight(1.00f + InfoPanel.getInfoHeight());
			}
		}
	}

	// -----------------
	// CALCULATE FITNESS
	// -----------------

	private void calculateFitness() {
		Block entry = Main.world.getEntryPoint();
		Block exit = Main.world.getExitPoint();

		double npDelta = Math.sqrt(PApplet.sq(exit.getX() - entry.getX()) + PApplet.sq(exit.getY() - entry.getY()));
		double xpDelta = Math.sqrt(PApplet.sq(exit.getX() - x) + PApplet.sq(exit.getY() - y));

		fitness = (int) ((npDelta - xpDelta) / npDelta * 100.0);
	}

	// ------
	// MUTATE
	// ------

	public void mutate() {
		// Add new behaviour
		if (Math.random() > 0.50) {
			brain.add(new Neuron(this));
		}
		// Remove old behaviour
		else {
			brain.remove(Math.floor(Math.random() * brain.size()));
		}
	}

	// ---------
	// REPRODUCE
	// ---------

	public Creature reproduce(int x, int y) {
		// Copy from parent
		Creature c = new Creature(x, y, col);
		c.setParent(parent);
		c.setGeneration(generation + 1);

		for (Neuron neu : brain) {
			c.getBrain().add(neu.copy(c));
		}

		// Add new features
		for (int i = 0; i < CHILD_MUTATIONS; i++) {
			if (Math.random() < MUTATION_CHANCE) {
				c.mutate();
			}
		}

		return c;
	}

	public static Creature reproduce(int x, int y, Creature p1, Creature p2) {
		Creature c = new Creature(x, y, p.lerpColor(p1.getCol(), p2.getCol(), 0.5f));
		c.setGeneration(Math.max(p1.getGeneration(), p2.getGeneration()) + 1);

		for (Neuron neu : p1.getBrain()) {
			if (Math.random() > 0.50) {
				c.getBrain().add(neu.copy(c));
			}
		}

		for (Neuron neu : p2.getBrain()) {
			if (Math.random() > 0.50) {
				c.getBrain().add(neu.copy(c));
			}
		}

		return c;
	}

	// -----------
	// GET AND SET
	// -----------

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getHealth() {
		return health;
	}

	public void setHealth(byte health) {
		this.health = health;
	}

	public byte getEnergy() {
		return energy;
	}

	public void setEnergy(byte energy) {
		this.energy = energy;
	}

	public int getFitness() {
		return fitness;
	}

	public void setFitness(int fitness) {
		this.fitness = fitness;
	}

	public int getAge() {
		return age;
	}

	public int getGeneration() {
		return generation;
	}

	public void setGeneration(int generation) {
		this.generation = generation;
	}

	private void setParent(Creature parent) {
		this.parent = parent;
	}

	private ArrayList<Neuron> getBrain() {
		return brain;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
