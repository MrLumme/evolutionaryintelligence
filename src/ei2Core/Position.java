package ei2Core;

import processing.core.PApplet;

public abstract class Position {

	protected static final PApplet p = Main.instance;

	// ---------
	// VARIABLES
	// ---------

	protected int x, y;
	protected int displayX, displayY;
	protected int col;

	// -----------
	// CONSTRUCTOR
	// -----------

	protected Position(int x, int y, int col) {
		this.x = x;
		this.y = y;
		this.col = col;
	}

	// ----
	// DRAW
	// ----

	public void draw() {
		displayX = x;
		displayY = y;

		drawAt(x, y);
	}

	public void draw(int x, int y) {
		displayX = x;
		displayY = y;

		drawAt(x, y);
	}

	// -------
	// DRAW AS
	// -------

	private void drawAt(int x, int y) {
		p.noStroke();
		p.fill(col);
		p.rect(x * Main.BLOCK_SIZE, y * Main.BLOCK_SIZE, Main.BLOCK_SIZE, Main.BLOCK_SIZE);
	}

	// -----------
	// GET AND SET
	// -----------

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int[] getPosition() {
		int[] out = { x, y };
		return out;
	}

	public void setPosition(int[] pos) {
		this.x = pos[0];
		this.y = pos[1];
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getDisplayX() {
		return displayX;
	}

	public void setDisplayX(int displayX) {
		this.displayX = displayX;
	}

	public int getDisplayY() {
		return displayY;
	}

	public void setDisplayY(int displayY) {
		this.displayY = displayY;
	}

}
