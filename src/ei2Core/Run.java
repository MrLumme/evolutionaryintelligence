package ei2Core;

import processing.core.PApplet;

public final class Run {

	// ---------
	// JAVA MAIN
	// ---------

	public static void main(String[] args) {
		PApplet.main("ei2Core.Main");
	}
}
