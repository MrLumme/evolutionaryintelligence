package ei2Core;

import ei2Types.BlockType;

public class Block extends Position {

	// ---------
	// VARIABLES
	// ---------

	private BlockType type;
	private boolean opaque;

	// -----------
	// CONSTRUCTOR
	// -----------

	Block(int x, int y, int col, BlockType type, boolean opaque) {
		super(x, y, col);

		this.type = type;
		this.opaque = opaque;
	}

	// ----
	// INFO
	// ----

	void info() {
		// Tell me what you are
		p.text("Block:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
		p.text(BlockType.getTypeName(type), (Main.WORLD_WIDTH + 4) * Main.BLOCK_SIZE,
				Main.BLOCK_SIZE * InfoPanel.getInfoHeight());
	}

	// ----
	// COPY
	// ----

	Block copy() {
		return new Block(x, y, col, type, opaque);
	}

	Block copy(int x, int y) {
		return new Block(x, y, col, type, opaque);
	}

	public BlockType getType() {
		return type;
	}

	public boolean isOpaque() {
		return opaque;
	}
}
