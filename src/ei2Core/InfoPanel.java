package ei2Core;

import ei2Types.ColorTypes;
import ei2Types.CreatureSetting;
import ei2Types.MapSetting;
import ei2Types.ProgramState;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PFont;

public class InfoPanel {

	private static PApplet p = Main.instance;
	private static InfoPanel instance;

	// ---------
	// CONSTANTS
	// ---------

	private static final float infoBase = 1.95f;

	// ---------
	// VARIABLES
	// ---------

	private static float infoHeight = 0;
	private static PFont infoFont = null;

	// -----------
	// CONSTRUCTOR
	// -----------

	private InfoPanel() {
		infoFont = p.loadFont("resources/infoText.vlw");
	}

	// ------------
	// GET INSTANCE
	// ------------

	public static InfoPanel getInstance() {
		if (instance == null) {
			instance = new InfoPanel();
		}

		return instance;
	}

	// ----
	// DRAW
	// ----

	public static void draw() {
		infoHeight = infoBase;

		// Setup text
		p.textFont(infoFont, Main.BLOCK_SIZE);
		p.fill(ColorTypes.WHITE);
		p.textAlign(PConstants.LEFT, PConstants.BOTTOM);

		// Draw panel background
		p.fill(ColorTypes.BLACK);
		p.rect(Main.WORLD_WIDTH * Main.BLOCK_SIZE, 0, (Main.WORLD_WIDTH + Main.TEXT_WIDTH) * Main.BLOCK_SIZE,
				Main.WORLD_HEIGHT * Main.BLOCK_SIZE);

		// Show pause
		if (Main.pause == true) {
			p.fill(ColorTypes.RED);
			p.text("Paused", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		} else {
			p.fill(ColorTypes.GREEN);
			p.text("Mainning", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		}

		// Show FPS
		p.fill(ColorTypes.WHITE);
		p.text("FPS:", (Main.WORLD_WIDTH + 8) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Math.round(p.frameRate), (Main.WORLD_WIDTH + 11) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

		infoHeight += 1.00;

		p.text("State:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		if (Main.ps == ProgramState.SIMULATION) {
			drawForSimulation();
		} else if (Main.ps == ProgramState.MAIN_MENU) {
			drawForMainMenu();
		} else if (Main.ps == ProgramState.MAP) {
			drawForMap();
		} else if (Main.ps == ProgramState.GENERATION) {
			drawForGeneration();
		} else if (Main.ps == ProgramState.SETUP) {
			drawForSetup();
		}
	}

	// -------------------
	// DRAW FOR SIMULATION
	// -------------------

	private static void drawForSimulation() {
		p.fill(ColorTypes.YELLOW);
		p.text("Simulation", (Main.WORLD_WIDTH + 5) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Show simulation speed
		p.text("Speed:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Main.stepLength, (Main.WORLD_WIDTH + 10) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

		infoHeight += 1.00;
		infoHeight += 1.00;

		// Show world info
		p.text("Creature count:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Main.world.getEpochSize(), (Main.WORLD_WIDTH + 10) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

		infoHeight += 1.00;

		// Show world info
		p.text("Simulation ID:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Main.world.getSimulationID(), (Main.WORLD_WIDTH + 10) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

		infoHeight += 1.00;
		infoHeight += 1.00;

		// Show creature info
		Creature c = Main.world.getCreatureAt(p.mouseX, p.mouseY, true);

		if (c != null) {
			c.info();
		}
	}

	// ------------------
	// DRAW FOR MAIN MENU
	// ------------------

	private static void drawForMainMenu() {
		p.fill(ColorTypes.YELLOW);
		p.text("Main menu", (Main.WORLD_WIDTH + 5) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Draw options
		p.text("'M' for map screen", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

		infoHeight += 1.00;
		infoHeight += 1.00;

		p.fill(Main.world.isAvailable() ? ColorTypes.WHITE : ColorTypes.D_GREY);
		p.text("'S' for simulation", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;

		if (Main.world.isAvailable() == false) {
			p.fill(ColorTypes.YELLOW);
			p.text("No map available", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
			p.fill(ColorTypes.WHITE);
		}

		infoHeight += 1.00;
		p.text("'Q' for quick start", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
	}

	// ------------
	// DRAW FOR MAP
	// ------------

	private static void drawForMap() {
		p.fill(ColorTypes.YELLOW);
		p.text("Map", (Main.WORLD_WIDTH + 5) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Draw options

		p.text("'L' to load map", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.fill(Main.world.isAvailable() ? ColorTypes.WHITE : ColorTypes.D_GREY);
		p.text("'S' to save map", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Drawing Toggle

		p.text("'D' to toggle drawing", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.text("Drawing:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

		p.fill((Main.mapDrawing == true ? ColorTypes.GREEN : ColorTypes.RED));
		p.text((Main.mapDrawing == true ? "Enabled" : "Disabled"), (Main.WORLD_WIDTH + 6) * Main.BLOCK_SIZE,
				Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		p.fill((Main.mapDrawing == true ? ColorTypes.WHITE : ColorTypes.D_GREY));
		p.text("'B' to switch block type", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.text("Selected: ", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

		String select;
		switch (Main.mapDrawSelect) {
		case WALL:
			select = "Wall";
			break;
		case MUD:
			select = "Mud";
			break;
		default:
			select = "Empty";
			break;
		}
		p.fill((Main.mapDrawing == true ? ColorTypes.YELLOW : ColorTypes.D_GREY));
		p.text(select, (Main.WORLD_WIDTH + 6) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);

		infoHeight += 1.00;
		infoHeight += 1.00;

		// Generation Settings

		p.text("'A' to switch settings", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		p.fill(Main.mapSetting == MapSetting.WALL_DENSITY ? ColorTypes.YELLOW : ColorTypes.WHITE);
		p.text("Wall density:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Main.world.WALL_DENSITY, (Main.WORLD_WIDTH + 10) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.fill(Main.mapSetting == MapSetting.MUD_DENSITY ? ColorTypes.YELLOW : ColorTypes.WHITE);
		p.text("Mud density:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Main.world.MUD_DENSITY, (Main.WORLD_WIDTH + 10) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.fill(Main.mapSetting == MapSetting.PLANT_DENSITY ? ColorTypes.YELLOW : ColorTypes.WHITE);
		p.text("Plant density:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Main.world.PLANT_DENSITY, (Main.WORLD_WIDTH + 10) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		p.text("'G' to generate new map", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Return to main

		p.text("'Q' to go back", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

	}

	// -------------------
	// DRAW FOR GENERATION
	// -------------------

	private static void drawForGeneration() {
		p.fill(ColorTypes.YELLOW);
		p.text("Generation", (Main.WORLD_WIDTH + 5) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Show world info
		p.text("Generation ID:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Main.world.getGenerationID(), (Main.WORLD_WIDTH + 10) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		// Auto Run
		p.fill(AutoRunner.isRunning() ? ColorTypes.GREEN : ColorTypes.RED);
		p.text("'R' to auto run", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Draw options
		p.text("'S' to sort generation", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.text("'A' to switch alignment", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.text("'M' to switch sort mode", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Draw Sort Settings
		p.text("Sorting for:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		String sortSettings = "";
		switch (Main.sortFor) {
		case ID:
			sortSettings += "ids, ";
			break;
		case NAME:
			sortSettings += "names, ";
			break;
		case AGE:
			sortSettings += "ages, ";
			break;
		case FITNESS:
			sortSettings += "fitness, ";
			break;
		}

		if (Main.sortHighest) {
			sortSettings += "high to low";
		} else {
			sortSettings += "low to high";
		}

		p.text(sortSettings, (Main.WORLD_WIDTH + 1) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Auto-select half
		p.text("'H' to auto-select half", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		// Kill selection
		p.text("'K' to kill selection", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		// Get new generation
		p.text("'G' to get new generation", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Simulate
		if (Main.world.isFinished() == true) {
			p.fill(ColorTypes.GREY);
		}
		p.text("'I' to simulate", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Show creature info
		Creature c = Main.world.getCreatureAt(p.mouseX, p.mouseY);

		if (c != null) {
			c.info();
		}

	}

	// --------------
	// DRAW FOR SETUP
	// --------------

	private static void drawForSetup() {
		p.fill(ColorTypes.YELLOW);
		p.text("Setup", (Main.WORLD_WIDTH + 5) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Draw options

		p.text("'S' to start simulation", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Creature Settings

		p.text("'A' to switch settings", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		p.fill(Main.creatureSetting == CreatureSetting.REPRODUCTION_CHANCE ? ColorTypes.YELLOW : ColorTypes.WHITE);
		p.text("Reproduction chance:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Creature.REPRODUCTION_CHANCE, (Main.WORLD_WIDTH + 12) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.fill(Main.creatureSetting == CreatureSetting.MUTATION_CHANCE ? ColorTypes.YELLOW : ColorTypes.WHITE);
		p.text("Mutation chance:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Creature.MUTATION_CHANCE, (Main.WORLD_WIDTH + 12) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		p.fill(Main.creatureSetting == CreatureSetting.CHILD_MUTATIONS ? ColorTypes.YELLOW : ColorTypes.WHITE);
		p.text("Child mutations:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Creature.CHILD_MUTATIONS, (Main.WORLD_WIDTH + 12) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.fill(Main.creatureSetting == CreatureSetting.SPAWN_MUTATIONS ? ColorTypes.YELLOW : ColorTypes.WHITE);
		p.text("Spawn mutations:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Creature.SPAWN_MUTATIONS, (Main.WORLD_WIDTH + 12) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		p.fill(Main.creatureSetting == CreatureSetting.CREATURE_COUNT ? ColorTypes.YELLOW : ColorTypes.WHITE);
		p.text("Creature count:", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		p.text(Main.CREATURE_COUNT, (Main.WORLD_WIDTH + 12) * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

		p.fill(ColorTypes.WHITE);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Set start and target

		p.text("'N' to set Entry to mouse position", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;

		p.text("'X' to set Exit to mouse position", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);
		infoHeight += 1.00;
		infoHeight += 1.00;

		// Return to main

		p.text("'Q' to go back", Main.WORLD_WIDTH * Main.BLOCK_SIZE, Main.BLOCK_SIZE * infoHeight);

	}

	// -----------
	// GET AND SET
	// -----------

	public static float getInfoHeight() {
		return infoHeight;
	}

	public static void setInfoHeight(float infoHeight) {
		InfoPanel.infoHeight = infoHeight;
	}

	public static PFont getInfoFont() {
		return infoFont;
	}
}
