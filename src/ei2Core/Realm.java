package ei2Core;

import java.util.ArrayList;
import java.util.Iterator;

import ei2Types.BlockType;
import ei2Types.ColorTypes;
import ei2Types.CreatureSort;
import ei2Types.ProgramState;
import processing.core.PApplet;

public class Realm {

	private static final PApplet p = Main.instance;

	// --------
	// SETTINGS
	// --------

	public float WALL_DENSITY = 0.25f;
	public float PLANT_DENSITY = 0.02f;
	public float MUD_DENSITY = 0.00f;

	// -----------
	// BLOCK TYPES
	// -----------

	private static Block blockEmpty, blockWall, blockMud;

	// --------
	// VARIBLES
	// --------

	private final ArrayList<Epoch> generations = new ArrayList<>();

	private boolean available;
	private boolean finished;
	private Block[][] map;
	private Block entryPoint, exitPoint;
	private int simulationID;
	private int generationID;
	private Epoch epoch;

	// -----------
	// CONSTRUCTOR
	// -----------

	public Realm() {
		map = new Block[Main.WORLD_WIDTH][Main.WORLD_HEIGHT];
		setupBlockTemplates();

		available = false;
		finished = false;
		simulationID = 0;
		generationID = 0;
		epoch = new Epoch();
		generations.add(epoch);

		entryPoint = new Block(0, 0, ColorTypes.RED, null, true);
		exitPoint = new Block(0, 0, ColorTypes.GREEN, null, true);
	}

	// --------
	// SIMULATE
	// --------

	public void simulate() {
		if (available) {
			simulationID = 0;
			epoch.get(simulationID).setActive(true);
		}
	}

	// ----
	// STEP
	// ----

	public void step(ProgramState ps) {
		if (ps == ProgramState.SIMULATION) {
			epoch.get(simulationID).step();
		}
	}

	// ----
	// DRAW
	// ----

	public void draw() {
		for (int x = 0; x < Main.WORLD_WIDTH; x++) {
			for (int y = 0; y < Main.WORLD_HEIGHT; y++) {
				map[x][y].draw();
			}
		}
	}

	public void draw(ProgramState ps) {
		if (ps == ProgramState.SIMULATION) {
			for (int x = 0; x < Main.WORLD_WIDTH; x++) {
				for (int y = 0; y < Main.WORLD_HEIGHT; y++) {
					map[x][y].draw();
				}
			}

			epoch.get(simulationID).draw();

			// Creature dead
			if (epoch.get(simulationID).getAge() > Main.MAX_AGE) {
				epoch.get(simulationID).setActive(false);
				simulationID++;

				// If last creature
				if (simulationID == epoch.size()) {
					finished = true;
				} else {
					epoch.get(simulationID).setActive(true);
				}
			}
		} else if (ps == ProgramState.GENERATION) {
			for (int i = 0; i < epoch.size(); i++) {
				epoch.get(i).draw((i % (Main.WORLD_WIDTH / 2 - 1)) * 2 + 1,
						PApplet.floor(i / (Main.WORLD_WIDTH / 2 - 1)) * 2 + 1, true);
			}
		} else if (ps == ProgramState.MAP || ps == ProgramState.SETUP || ps == ProgramState.MAIN_MENU) {
			if (available) {
				for (int x = 0; x < Main.WORLD_WIDTH; x++) {
					for (int y = 0; y < Main.WORLD_HEIGHT; y++) {
						map[x][y].draw();
					}
				}
			}
		}
	}

	// --------------
	// GENERATE WORLD
	// --------------

	public void generateWorld() {
		// Generate Plain
		for (int x = 0; x < Main.WORLD_WIDTH; x++) {
			for (int y = 0; y < Main.WORLD_HEIGHT; y++) {
				// Frame
				if (x == 0 || x == Main.WORLD_WIDTH - 1 || y == 0 || y == Main.WORLD_HEIGHT - 1) {
					map[x][y] = blockWall.copy(x, y);
				}

				// Mud
				else if (Math.random() < MUD_DENSITY) {
					map[x][y] = blockMud.copy(x, y);
				}

				// Wall
				else if (Math.random() < WALL_DENSITY) {
					map[x][y] = blockWall.copy(x, y);
				}

				// Empty
				else {
					map[x][y] = blockEmpty.copy(x, y);
				}
			}
		}

		// Fill Ones
		for (int x = 1; x < Main.WORLD_WIDTH - 1; x++) {
			for (int y = 1; y < Main.WORLD_HEIGHT - 1; y++) {

				if (map[x][y].isOpaque() == false && map[x + 1][y].isOpaque() == true
						&& map[x - 1][y].isOpaque() == true && map[x][y + 1].isOpaque() == true
						&& map[x][y - 1].isOpaque() == true) {

					map[x][y] = blockWall.copy(x, y);
				}
			}
		}
		available = true;
	}

	// -----------
	// CHECK EMPTY
	// -----------

	public boolean checkEmpty(int x, int y) {
		boolean state = true;

		if (map[x][y].isOpaque() == true) {
			state = false;
		}

		for (int j = 0; j < epoch.size(); j++) {
			if (epoch.get(j).getX() == x && epoch.get(j).getY() == y) {
				state = false;
				break;
			}
		}

		return state;
	}

	// --------------
	// FIND EMPTY POS
	// --------------

	public int[] findEmptyPos() {
		int time = p.millis();
		while (true) {
			int tempX = (byte) PApplet.floor(p.random(Main.WORLD_WIDTH));
			int tempY = (byte) PApplet.floor(p.random(Main.WORLD_HEIGHT));

			if (checkEmpty(tempX, tempY)) {
				int[] pos = { tempX, tempY };
				return pos;
			} else if (p.millis() > time + 100) {
				int[] pos = { -1, -1 };
				return pos;
			}
		}
	}

	// ------------
	// ADD CREATURE
	// ------------

	public void addCreature(Creature c) {
		epoch.add(c);
	}

	// ---------------
	// GET CREATURE AT
	// ---------------

	public Creature getCreatureAt(int x, int y) {

		Creature out = null;

		boolean found = false;
		Iterator<Creature> i = epoch.iterator();

		while (found == false && i.hasNext()) {

			Creature c = i.next();

			if (x >= (c.getDisplayX() * Main.BLOCK_SIZE) && x < (c.getDisplayX() * Main.BLOCK_SIZE) + Main.BLOCK_SIZE
					&& y >= (c.getDisplayY() * Main.BLOCK_SIZE)
					&& y < (c.getDisplayY() * Main.BLOCK_SIZE) + Main.BLOCK_SIZE) {
				found = true;
				out = c;
			}

		}

		return out;
	}

	public Creature getCreatureAt(int x, int y, boolean onlyActive) {

		Creature out = null;

		boolean found = false;
		Iterator<Creature> i = epoch.iterator();

		while (found == false && i.hasNext()) {

			Creature c = i.next();

			if (x >= (c.getDisplayX() * Main.BLOCK_SIZE) && x < (c.getDisplayX() * Main.BLOCK_SIZE) + Main.BLOCK_SIZE
					&& y >= (c.getDisplayY() * Main.BLOCK_SIZE)
					&& y < (c.getDisplayY() * Main.BLOCK_SIZE) + Main.BLOCK_SIZE) {
				if (onlyActive) {
					if (c.isActive()) {
						found = true;
						out = c;
					}
				} else {
					found = true;
					out = c;
				}
			}

		}

		return out;
	}

	// ---------------------
	// SETUP BLOCK TEMPLATES
	// ---------------------

	private static void setupBlockTemplates() {
		blockEmpty = new Block(0, 0, ColorTypes.GREY, BlockType.EMPTY, false);
		blockWall = new Block(0, 0, ColorTypes.BLACK, BlockType.WALL, true);
		blockMud = new Block(0, 0, ColorTypes.D_GREY, BlockType.MUD, false);
	}

	// ------------
	// SET BLOCK AT
	// ------------

	public void setBlockAt(int x, int y, BlockType bt) {
		switch (bt) {
		case EMPTY:
			map[x][y] = blockEmpty.copy(x, y);
			break;
		case MUD:
			map[x][y] = blockMud.copy(x, y);
			break;
		case WALL:
			map[x][y] = blockWall.copy(x, y);
			break;

		}
	}

	// -----------
	// IS BLOCK AT
	// -----------

	public boolean isBlockAt(int x, int y, BlockType bt) {
		return map[x][y].getType() == bt ? true : false;
	}

	// ---------------
	// SET ENTRY POINT
	// ---------------

	public void setEntryPoint(int x, int y) {
		map[entryPoint.getX()][entryPoint.getY()] = blockEmpty.copy(x, y);
		entryPoint.setX(x);
		entryPoint.setY(y);
		map[x][y] = entryPoint;
	}

	public void setEntryPoint(int[] pos) {
		map[entryPoint.getX()][entryPoint.getY()] = blockEmpty.copy(pos[0], pos[1]);
		entryPoint.setPosition(pos);
		map[pos[0]][pos[1]] = entryPoint;
	}

	// --------------
	// SET EXIT POINT
	// --------------

	public void setExitPoint(int x, int y) {
		map[exitPoint.getX()][exitPoint.getY()] = blockEmpty.copy(x, y);
		exitPoint.setX(x);
		exitPoint.setY(y);
		map[x][y] = exitPoint;
	}

	public void setExitPoint(int[] pos) {
		map[exitPoint.getX()][exitPoint.getY()] = blockEmpty.copy(pos[0], pos[1]);
		exitPoint.setPosition(pos);
		map[pos[0]][pos[1]] = exitPoint;
	}

	// ----------
	// EXPORT MAP
	// ----------

	public String[] exportMap() {

		String[] file = new String[0];

		file = PApplet.append(file, "WORLD_WIDTH:" + Main.WORLD_WIDTH);
		file = PApplet.append(file, "WORLD_HEIGHT:" + Main.WORLD_HEIGHT);

		for (int x = 0; x < Main.WORLD_WIDTH; x++) {

			String line = "";

			for (int y = 0; y < Main.WORLD_HEIGHT; y++) {
				char block;
				if (map[x][y].getType() == BlockType.WALL) {
					block = 'W';
				} else if (map[x][y].getType() == BlockType.MUD) {
					block = 'M';
				} else if (map[x][y] == entryPoint) {
					block = 'N';
				} else if (map[x][y] == exitPoint) {
					block = 'X';
				} else {
					block = 'E';
				}
				line += block;
			}

			file = PApplet.append(file, line);
		}
		return file;
	}

	// ----------
	// IMPORT MAP
	// ----------

	public void importMap(String[] file) {

		if (file[0].equals("WORLD_WIDTH:" + Main.WORLD_WIDTH) && file[1].equals("WORLD_HEIGHT:" + Main.WORLD_HEIGHT)) {

			for (int x = 0; x < Main.WORLD_WIDTH; x++) {
				for (int y = 0; y < Main.WORLD_HEIGHT; y++) {
					if (file[x + 2].charAt(y) == 'W') {
						map[x][y] = blockWall.copy(x, y);
					} else if (file[x + 2].charAt(y) == 'M') {
						map[x][y] = blockMud.copy(x, y);
					} else if (file[x + 2].charAt(y) == 'N') {
						setEntryPoint(x, y);
					} else if (file[x + 2].charAt(y) == 'X') {
						setExitPoint(x, y);
					} else {
						map[x][y] = blockEmpty.copy(x, y);
					}
				}
			}
		}
		available = true;
	}

	// ---------------
	// Sort Generation
	// ---------------

	public void sortGeneration(CreatureSort sortFor, boolean sortHighest) {
		epoch.sort(sortFor, sortHighest);
	}

	// ----------------
	// Auto-select Half
	// ----------------

	public void autoSelect() {
		for (int i = 0; i < epoch.size(); i++) {
			epoch.get(i).setSelected(false);
		}
		for (int i = 0; i < epoch.size(); i++) {
			float chance = (i + 1.0f) / epoch.size();
			if (Math.random() < chance) {
				epoch.get(i).setSelected(true);
			}
		}
	}

	// --------------
	// Kill Selection
	// --------------

	public void killSelection() {
		Iterator<Creature> it = epoch.iterator();
		while (it.hasNext()) {
			Creature c = it.next();
			if (c.isSelected()) {
				it.remove();
			}
		}
	}

	// --------------
	// NEW GENERATION
	// --------------

	public void newGeneration() {

		Epoch parents = epoch;

		epoch = new Epoch();
		generationID++;
		generations.add(epoch);

		int i = 0;
		while (epoch.size() < Main.CREATURE_COUNT) {
			System.out.println(((float) (i + 1) / (float) parents.size()) < Math.random());

			if (((float) (i + 1) / (float) parents.size()) < Math.random()) {
				Creature p1 = parents.get(i);
				Creature p2 = parents.get((int) (Math.random() * parents.size()));

				Creature c = Creature.reproduce(entryPoint.getX(), entryPoint.getY(), p1, p2);
				epoch.add(c);
			}
			i = (i + 1) % epoch.size();
		}

		finished = false;
	}

	// -----------
	// GET AND SET
	// -----------

	public Block getEntryPoint() {
		return entryPoint;
	}

	public Block getExitPoint() {
		return exitPoint;
	}

	public boolean isAvailable() {
		return available;
	}

	public boolean isFinished() {
		return finished;
	}

	public int getEpochSize() {
		return epoch.size();
	}

	public int getSimulationID() {
		return simulationID;
	}

	public int getGenerationID() {
		return generationID;
	}
}
