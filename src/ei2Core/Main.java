package ei2Core;

import java.io.File;

import ei2Types.BlockType;
import ei2Types.ColorTypes;
import ei2Types.CreatureSetting;
import ei2Types.CreatureSort;
import ei2Types.MapSetting;
import ei2Types.ProgramState;
import processing.core.PApplet;

public final class Main extends PApplet {

	public static Main instance = null;

	// ---------
	// CONSTANTS
	// ---------

	// World constants
	public static final byte WORLD_WIDTH = 48;
	public static final byte WORLD_HEIGHT = 48;
	public static final byte BLOCK_SIZE = 16;
	public static final byte TEXT_WIDTH = 24;

	// Program constants
	public static boolean NO_DECAY = true;

	// Plant constants
	public static final float PLANT_CHANCE = 0.05f;

	// Food constants
	public static final float DECAY_MULTIPLIER = 2.0f;
	public static final float DECAY_RANGE = 1.5f;
	public static final float DECAY_MIN = 0.5f;

	// --------
	// SETTINGS
	// --------

	// Simulation settings
	public static int MAX_AGE = 50;
	public static int CREATURE_COUNT = 20;

	// ---------
	// VARIABLES
	// ---------

	// Program States
	public static ProgramState ps = ProgramState.MAIN_MENU;
	public static long steps = 0;
	public static boolean pause = false;
	public static boolean mapDrawing = false;
	public static BlockType mapDrawSelect = BlockType.EMPTY;
	public static MapSetting mapSetting = MapSetting.NONE;
	public static CreatureSetting creatureSetting = CreatureSetting.NONE;
	public static boolean sortHighest = true;
	public static CreatureSort sortFor = CreatureSort.FITNESS;

	// Timer
	public static int stepLength = 5;
	public long stepTimer = millis();

	// World
	public static Realm world;

	// --------
	// SETTINGS
	// --------

	@Override
	public void settings() {
		size((WORLD_WIDTH + TEXT_WIDTH) * BLOCK_SIZE, WORLD_HEIGHT * BLOCK_SIZE);
		instance = this;
	}

	// -----
	// SETUP
	// -----

	@Override
	public void setup() {
		frameRate(240);

		InfoPanel.getInstance();
		AutoRunner.getInstance();

		world = new Realm();
	}

	// ----
	// DRAW
	// ----

	@Override
	public void draw() {
		background(ColorTypes.BLACK);

		if (mousePressed) {
			mouseHold();
		}

		// Main Menu
		if (ps == ProgramState.MAIN_MENU || ps == ProgramState.MAP || ps == ProgramState.SETUP) {
			world.draw(ps);
		}

		// Simulation Screen
		else if (ps == ProgramState.SIMULATION) {

			// Step Event
			if (millis() - stepTimer >= stepLength && pause == false) {
				stepTimer = millis();
				stepEvent();
			}

			// Draw Map
			world.draw(ProgramState.SIMULATION);

			if (world.isFinished()) {
				ps = ProgramState.GENERATION;
			}
		}

		// Generation Screen
		else if (ps == ProgramState.GENERATION) {
			world.draw(ps);
		}

		// Draw Info Panel
		InfoPanel.draw();

		// Step Auto Runner
		AutoRunner.step();
	}

	// ----------
	// MOUSE HOLD
	// ----------

	public void mouseHold() {
		if (ps == ProgramState.MAP && mapDrawing) {
			if (mouseX < (WORLD_WIDTH * BLOCK_SIZE) && mouseX > 0 && mouseY < (WORLD_HEIGHT * BLOCK_SIZE)
					&& mouseY > 0) {
				int x = mouseX / BLOCK_SIZE, y = mouseY / BLOCK_SIZE;
				world.setBlockAt(x, y, mapDrawSelect);
			}
		}
	}

	// -------------
	// MOUSE PRESSED
	// -------------

	@Override
	public void mousePressed() {
		if (ps == ProgramState.GENERATION) {
			Creature c = world.getCreatureAt(mouseX, mouseY);
			if (c != null) {
				c.setSelected(!c.isSelected());
			}
		}
	}

	// -----------
	// KEY PRESSED
	// -----------

	@Override
	public void keyPressed() {
		// Keys For Simulation
		if (ps == ProgramState.SIMULATION) {
			if (key == 'p' || key == ' ') {
				pause = !pause;
			} else if (key == CODED && keyCode == LEFT) {
				stepLength += stepLength > 1 ? -1 : 0;
			} else if (key == CODED && keyCode == RIGHT) {
				stepLength += 1;
			}
		}

		// Keys For Main Menu
		else if (ps == ProgramState.MAIN_MENU) {
			if (key == 'm' || key == ' ') {
				ps = ProgramState.MAP;
			} else if (key == 's' && world.isAvailable()) {
				ps = ProgramState.SETUP;
			} else if (key == 'q') {
				world.generateWorld();

				world.setEntryPoint(world.findEmptyPos());

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// Do nothing
				}

				world.setExitPoint(world.findEmptyPos());

				ps = ProgramState.SETUP;
			}
		}

		// Keys For Generation Screen
		else if (ps == ProgramState.GENERATION) {
			if (key == 'a') {
				// Switch Sorting Arrangement
				sortHighest = !sortHighest;
			} else if (key == 'm') {
				// Switch Sorting Mode
				sortFor = CreatureSort.getNext(sortFor);
			} else if (key == 's') {
				// Go Sort
				world.sortGeneration(sortFor, sortHighest);
			} else if (key == 'h') {
				// Auto-select Half
				world.autoSelect();
			} else if (key == 'k') {
				// Kill selected
				world.killSelection();
			} else if (key == 'g') {
				// New generation
				world.newGeneration();
			} else if (key == 'i') {
				// Simulate next generation
				if (world.isFinished() == false) {
					ps = ProgramState.SIMULATION;
					world.simulate();
				}
			} else if (key == 'r') {
				// Enable / Disable Auto Runner
				AutoRunner.setRunning(!AutoRunner.isRunning());
			}
		}

		// Keys For Setup Screen
		else if (ps == ProgramState.SETUP) {
			if (key == 'a') {
				// Switch creature settings
				creatureSetting = CreatureSetting.getNext(creatureSetting);
			} else if (key == 'q') {
				// Back to main menu
				ps = ProgramState.MAIN_MENU;
			} else if (key == 'n') {
				// Set start position
				if (mouseX < (WORLD_WIDTH * BLOCK_SIZE) && mouseX > 0 && mouseY < (WORLD_HEIGHT * BLOCK_SIZE)
						&& mouseY > 0) {
					int x = mouseX / BLOCK_SIZE, y = mouseY / BLOCK_SIZE;

					world.setEntryPoint(x, y);
				}
			} else if (key == 'x') {
				// Set target position
				if (mouseX < (WORLD_WIDTH * BLOCK_SIZE) && mouseX > 0 && mouseY < (WORLD_HEIGHT * BLOCK_SIZE)
						&& mouseY > 0) {
					int x = mouseX / BLOCK_SIZE, y = mouseY / BLOCK_SIZE;

					world.setExitPoint(x, y);
				}
			} else if (key == 's') {
				// Start simulation
				ps = ProgramState.SIMULATION;

				for (int i = 0; i < CREATURE_COUNT; i++) {
					Block entry = world.getEntryPoint();
					Creature c = new Creature(entry.getX(), entry.getY(), color((int) (Math.random() * 225 + 20),
							(int) (Math.random() * 225 + 20), (int) (Math.random() * 225 + 20)));

					for (int j = 0; j < Creature.SPAWN_MUTATIONS; j++) {
						if (Math.random() < Creature.MUTATION_CHANCE) {
							c.mutate();
						}
					}

					world.addCreature(c);
				}
				world.simulate();

			} else if (key == CODED && keyCode == LEFT) {
				switch (creatureSetting) {
				case REPRODUCTION_CHANCE:
					Creature.REPRODUCTION_CHANCE = (float) Math.max(0.00, Creature.REPRODUCTION_CHANCE - 0.01);
					break;
				case MUTATION_CHANCE:
					Creature.MUTATION_CHANCE = (float) Math.max(0.00, Creature.MUTATION_CHANCE - 0.01);
					break;
				case CHILD_MUTATIONS:
					Creature.CHILD_MUTATIONS = Math.max(0, Creature.CHILD_MUTATIONS - 1);
					break;
				case SPAWN_MUTATIONS:
					Creature.SPAWN_MUTATIONS = Math.max(0, Creature.SPAWN_MUTATIONS - 1);
					break;
				case CREATURE_COUNT:
					CREATURE_COUNT = Math.max(0, CREATURE_COUNT - 1);
					break;
				default:
					break;
				}
			} else if (key == CODED && keyCode == RIGHT) {
				switch (creatureSetting) {
				case REPRODUCTION_CHANCE:
					Creature.REPRODUCTION_CHANCE = (float) Math.min(1.000, Creature.REPRODUCTION_CHANCE + 0.01);
					break;
				case MUTATION_CHANCE:
					Creature.MUTATION_CHANCE = (float) Math.min(1.000, Creature.MUTATION_CHANCE + 0.01);
					break;
				case CHILD_MUTATIONS:
					Creature.CHILD_MUTATIONS = Math.min(100, Creature.CHILD_MUTATIONS + 1);
					break;
				case SPAWN_MUTATIONS:
					Creature.SPAWN_MUTATIONS = Math.min(100, Creature.SPAWN_MUTATIONS + 1);
					break;
				case CREATURE_COUNT:
					CREATURE_COUNT = CREATURE_COUNT + 1;
					break;
				default:
					break;
				}
			}
		}

		// Keys For Map Screen
		else if (ps == ProgramState.MAP) {
			if (key == 'g') {
				// Generate Map
				world.generateWorld();
			} else if (key == 'l') {
				// Load Map
				selectInput("Load .eim file...", "loadMap");
			} else if (key == 's' && world.isAvailable()) {
				// Save Map
				selectOutput("Save .eim file...", "saveMap");
			} else if (key == 'd' && world.isAvailable()) {
				// Toggle Drawing
				mapDrawing = !mapDrawing;
			} else if (key == 'a') {
				// Map setting
				mapSetting = MapSetting.getNext(mapSetting);
			} else if (key == 'b') {
				// Drawing select
				mapDrawSelect = BlockType.getNext(mapDrawSelect);
			} else if (key == 'q') {
				// Back to main menu
				ps = ProgramState.MAIN_MENU;
			} else if (key == CODED && keyCode == LEFT) {
				// Change map setting
				switch (mapSetting) {
				case WALL_DENSITY:
					world.WALL_DENSITY = (float) Math.max(0.000, world.WALL_DENSITY - 0.01);
					break;
				case MUD_DENSITY:
					world.MUD_DENSITY = (float) Math.max(0.000, world.MUD_DENSITY - 0.01);
					break;
				case PLANT_DENSITY:
					world.PLANT_DENSITY = (float) Math.max(0.000, world.PLANT_DENSITY - 0.01);
					break;
				default:
					break;
				}
			} else if (key == CODED && keyCode == RIGHT) {
				// Change map setting
				switch (mapSetting) {
				case WALL_DENSITY:
					world.WALL_DENSITY = (float) Math.min(1.000, world.WALL_DENSITY + 0.01);
					break;
				case MUD_DENSITY:
					world.MUD_DENSITY = (float) Math.min(1.000, world.MUD_DENSITY + 0.01);
					break;
				case PLANT_DENSITY:
					world.PLANT_DENSITY = (float) Math.min(1.000, world.PLANT_DENSITY + 0.01);
					break;
				default:
					break;
				}
			}
		}
	}

	// ----------
	// STEP EVENT
	// ----------

	private void stepEvent() {
		steps++;

		world.step(ps);
	}

	// --------
	// LOAD MAP
	// --------

	public void loadMap(File load) {
		String path = load.getAbsolutePath();
		world.importMap(loadStrings(path));
	}

	// --------
	// SAVE MAP
	// --------

	public void saveMap(File save) {

		String path = save.getAbsolutePath();

		if (path.lastIndexOf(".eim") == -1) {
			path += ".eim";
		}

		saveStrings(path, world.exportMap());
	}
}
