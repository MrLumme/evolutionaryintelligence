package ei2Core;

import ei2Types.AbilityType;
import ei2Types.DirectionTypes;

public class Ability {

	AbilityType type;
	byte direction;
	byte consumption;
	Creature vessel;

	// -----------
	// CONSTRUCTOR
	// -----------

	public Ability(Creature vessel) {
		type = AbilityType.WALK; // AbilityType.getRandomType();

		direction = (byte) Math.floor(Math.random() * (DirectionTypes.DIR_AMOUNT));

		this.vessel = vessel;
	}

	public Ability(AbilityType type, byte direction, Creature vessel) {
		this.type = type;

		this.direction = direction;

		this.vessel = vessel;
	}

	// ----
	// STEP
	// ----

	public boolean step() {
		boolean hasActed = false;

		switch (type) {
		case WALK:
			int x = vessel.getX() + DirectionTypes.DIR_PRESET[direction][0];
			int y = vessel.getY() + DirectionTypes.DIR_PRESET[direction][1];

			if (Main.world.checkEmpty(x, y)) {
				vessel.setX(x);
				vessel.setY(y);
				hasActed = true;
			}
			break;
		case EAT:
			break;
		case BUILD:
			break;
		case ATTACK:
			break;
		default:
			break;
		}

		return hasActed;
	}

	// ----
	// COPY
	// ----

	public Ability copy(Creature vessel) {
		return new Ability(type, direction, vessel);
	}

	// -----------
	// GET AND SET
	// -----------

	public AbilityType getType() {
		return type;
	}

	public void setType(AbilityType type) {
		this.type = type;
	}

	public byte getDirection() {
		return direction;
	}

	public void setDirection(byte direction) {
		this.direction = direction;
	}
}
