package ei2Core;

import ei2Types.ColorTypes;
import processing.core.PApplet;

public class Nutrients {

	private final static PApplet p = Main.instance;

	// ---------
	// CONSTANTS
	// ---------

	public static final byte PT = 0;
	public static final byte FR = 1;
	public static final byte GA = 2;
	public static final byte VE = 3;
	public static final byte DA = 4;
	public static final byte MG = 5;

	public static final byte NUTRIENT_AMOUNT = 6;
	public static final String NUTRIENT_NAME[] = { "PT", "FR", "GA", "VE", "DA", "MG" };
	public static final int NUTRIENT_COLOR[] = { ColorTypes.RED, ColorTypes.CYAN, ColorTypes.YELLOW, ColorTypes.GREEN,
			ColorTypes.BLUE, ColorTypes.MAGENTA };

	// ---------
	// VARIABLES
	// ---------

	private double[] set;

	// -----------
	// CONSTRUCTOR
	// -----------

	public Nutrients() {
		set = new double[6];
	}

	public Nutrients(double[] set) {
		this.set = set;
	}

	public Nutrients(double PT, double FR, double GA, double VE, double DA, double MG) {
		set[Nutrients.PT] = PT;
		set[Nutrients.FR] = FR;
		set[Nutrients.GA] = GA;
		set[Nutrients.VE] = VE;
		set[Nutrients.DA] = DA;
		set[Nutrients.MG] = MG;
	}

	// ----------------
	// GET NUTRIENT COL
	// ----------------

	public int getNutrientCol() {
		double r = set[PT] / 2 + set[GA] / 4 + set[MG] / 4;
		double g = set[VE] / 2 + set[GA] / 4 + set[FR] / 4;
		double b = set[DA] / 2 + set[MG] / 4 + set[FR] / 4;

		return p.color((int) (r * 255), (int) (g * 255), (int) (b * 255));
	}

	// ---
	// SUB
	// ---

	public boolean sub(byte nutrient, double amount) {
		boolean neg = false;

		set[nutrient] -= amount;

		if (set[nutrient] < 0.00) {
			neg = true;
			set[nutrient] = 0.00;
		}

		return neg;
	}

	public boolean sub(double amount) {
		boolean neg = false;

		for (int i = 0; i < NUTRIENT_AMOUNT; i++) {
			set[i] -= amount;

			if (set[i] < 0.00) {
				neg = true;
				set[i] = 0.00;
			}
		}

		return neg;
	}

	public boolean sub(double[] subSet, double amount) {
		boolean neg = false;

		for (int i = 0; i < NUTRIENT_AMOUNT; i++) {
			set[i] -= subSet[i];

			if (set[i] < 0.00) {
				neg = true;
				set[i] = 0.00;
			}
		}

		return neg;
	}

	// ---
	// ADD
	// ---

	public boolean add(byte nutrient, double amount) {
		boolean pos = false;

		set[nutrient] += amount;

		if (set[nutrient] > 1.00) {
			pos = true;
			set[nutrient] = 1.00;
		}

		return pos;
	}

	public boolean add(double amount) {
		boolean pos = false;

		for (int i = 0; i < NUTRIENT_AMOUNT; i++) {
			set[i] += amount;

			if (set[i] > 1.00) {
				pos = true;
				set[i] = 1.00;
			}
		}

		return pos;
	}

	public boolean add(double[] subSet, double amount) {
		boolean pos = false;

		for (int i = 0; i < NUTRIENT_AMOUNT; i++) {
			set[i] += subSet[i];

			if (set[i] > 1.00) {
				pos = true;
				set[i] = 1.00;
			}
		}

		return pos;
	}

	// ------------
	// GET NUTRIENT
	// ------------

	public double getNutrient(byte nutrient) {
		return set[nutrient];
	}

	// ------------
	// SET NUTRIENT
	// ------------

	public void setNutrient(byte nutrient, double amount) {
		set[nutrient] = amount;
	}
}
